#include <cstdlib>
#include <iostream>
#include <string>
#include <fstream>
using namespace std;

void matchcipher();
void shifter(int shiftin);
void manualcipher(int option);
void setup();
void subcipher();

struct ciphertextinfo {
       int length;
       string ciphertext;
       int numerics[];
       //int shiftstreams[26];       
};

struct deckey {
       int length;
       string base, offset, decrypted;
       char *basec, *offsetc;
};

ciphertextinfo ctinfo;
bool usecaps = true;

char capsalphabet[26] = {
     'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z' };
     //1  2   3   4   5   6   7   8   9   10  11  12  13  14  15  16  17  18  19  20  21  22  23  24  25  26
char alphabet[26] = {
     'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z' };


int main(int argc, char *argv[])
{
    int choice = 0;
    while(choice != -1)
    {
                 cout << "\t***Trithenium Cryptology Analysis System***" << endl; 
                 cout << "1. Decrypt Caesar Cipher from file\n";
                 cout << "2. Decrypt input Caesar String\n";
                 cout << "3. Encrypt input Caesar String\n"; 
                 cout << "4. Analyze substitution cipher\n";
                 cout << "5. Exit\n";               
                 cout << "Enter choice: ";
                 cin >> choice;
                 
                 switch(choice) {
                 case 1: cout << endl; setup(); break;
                 case 2: cout << endl; manualcipher(1); break;
                 case 3: cout << endl; manualcipher(2); break;
                 case 4: cout << endl; subcipher(); break;
                 case 5: system("PAUSE"); return EXIT_SUCCESS;     
                 }       
    }
}

void subcipher()
{
     
     
}

void manualcipher(int option)
{
     deckey decstruct;
     string line, filename, dfilename;
     char *efile, *dfile;
     cout << "Enter name of encrypted file: ";
     cin >> filename;
     cout << "\nEnter name of decryption key: ";
     cin >> dfilename;
     
     efile = new char [filename.size()+1];
     strcpy (efile, filename.c_str());
     
     dfile = new char [dfilename.size()+1];
     strcpy (dfile, dfilename.c_str());
     
     ifstream myfile(efile);
     ifstream decfile(dfile);
     
     if (myfile.is_open())
     {
         while (myfile.good())
         {
               getline (myfile, ctinfo.ciphertext);
               cout << "Cipher Text: " << ctinfo.ciphertext << endl;
         }
         myfile.close();
     }
     else cout << "Unable to open file";
     
     if (decfile.is_open())
     {
          while (decfile.good())
          {
                getline (decfile, decstruct.base);
                cout << "Base Text: " << decstruct.base << endl;
                
                getline (decfile, decstruct.offset);
                cout << "Offset Text: " << decstruct.offset << endl;
            }
            decfile.close();
     }     
     
     for(int i = 0; i < ctinfo.ciphertext.length(); i++)
     {
          char c = ctinfo.ciphertext.at(i);  
          cout << "char at loc " << i << " is " << c << endl;
          
          
          for(int i2 = 0; i2 < decstruct.base.length(); i2++)
          {
                  if ( c == decstruct.base[i2] )
                  {
                       decstruct.decrypted[i] = decstruct.offset[i2];
                  }
          } 
      } 
      
      cout << "Decrypted Text: " << decstruct.decrypted << endl;
}

void setup()
{
    string line;
    ifstream myfile("myfile.txt");
    
    if (myfile.is_open())
    {
           while ( myfile.good() )
           {
                 getline (myfile, ctinfo.ciphertext);
                 cout << "Cipher Text: " << ctinfo.ciphertext << endl;
                 
                 matchcipher();
                 //ctinfo.numerics.push_back(alphabet[1]);
                 cout << "Numerics:    ";
                 for(int i = 0; i < ctinfo.ciphertext.length(); i++)
                 { 
                         if ( ctinfo.numerics[i] == -1 ) cout << " ";
                         else cout << ctinfo.numerics[i];
                 }
                 cout << endl;
                 for(int i = 1; i < 26; i++) { cout << i << ": "; shifter(i); }
            }
            myfile.close();
     }                 
    else cout << "Unable to open file";     
}

void matchcipher()
{
     for(int i = 0; i < ctinfo.ciphertext.length(); i++)
     {
             char cpchar = ctinfo.ciphertext[i];
             //cout << cpchar << " ";
             if(cpchar == ' ')
             {
                       ctinfo.numerics[i] = -1;
                          //cout << "this char is a space" << endl;
             }
             else for(int j = 0; j < 26; j++)
             {
                     //cout << "checking cpchar: " << cpchar << " vs " << capsalphabet[j] << endl;
                     if(cpchar == capsalphabet[j])
                     {
                               usecaps = true;
                             //cout << "this char is a " << j+1 << endl;
                             ctinfo.numerics[i] = j+1;
                     }
                     else if (cpchar == alphabet[j])
                     {
                          usecaps = false;
                          ctinfo.numerics[i] = j+1;
                     }
                          
             }
      }
}

void shifter(int shiftin)
{
     char schar;
     for(int i = 0; i < ctinfo.ciphertext.length(); i++)
     {
             if (ctinfo.numerics[i] == -1) cout << " ";
             else {
             int sint = (ctinfo.numerics[i] + shiftin) %26;
             if (sint == 0) sint = 26;
             if(usecaps) schar = capsalphabet[sint-1];
             else schar = alphabet[sint-1];
             cout << schar; }       
     }
     cout << endl;
     
     
}
